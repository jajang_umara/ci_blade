<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Proyek extends Eloquent {
	protected $table = 'master.master_proyek';
}