<?php
use Jenssegers\Blade\Blade;

if (!function_exists('view')){
	function view($view,$data=array()){
		
		//View Path
		$path  = APPPATH.'views';
		
		// Blade instance
		$blade = new Blade($path,APPPATH.'cache/views');
		
		//Share Data
		$blade->share('title','Page Title');
		
		//Replace dot with slash
		$view  = str_replace('.','/',$view);
		
		echo $blade->make($view,$data);
	}
}