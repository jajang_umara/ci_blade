<?php

function _this()
{
	$ci =& get_instance();
	return $ci;
}

function _post($index,$default="")
{
	return isset($_POST[$index]) ? _this()->input->post($index,true) : $default;
}

function _get($index,$default="")
{
	return isset($_GET[$index]) ? _this()->input->get($index,true) : $default;
}

function session_get($key,$json=false)
{
	if ($json){
		$json = json_decode(_this()->session->userdata('akses'),true);
		return isset($json[$key]) ? $json[$key] : false;
	}
	return _this()->session->userdata($key);
}


function session_set($key,$value=null)
{
	if (is_array($key)){
		_this()->session->set_userdata($key);
	} else {
		_this()->session->set_userdata($key,$value);
	}
}

function load_db($db)
{
	return _this()->load->database($db,true);
}

function _uri($page,$segment=1,$class="active")
{
	if(is_array($page)){
		return in_array(_this()->uri->segment($segment),$page) ? $class : '';
	}
	
	return _this()->uri->segment($segment) == $page ? $class : '';
}

function _uri_array($array,$segment=1,$class='menu-open')
{
	return in_array(_this()->uri->segment($segment),$array) ? $class : '';
}

function __($text)
{
	return ucfirst(str_replace("_"," ",$text));
}

function format_tanggal_indonesia($date,$hari=true)
{
	$bulan = array('01'=>'Januari','02'=>'Februari','03'=>'Maret','04'=>'April','05'=>'Mei','06'=>'Juni','07'=>'Juli','08'=>'Agustus','09'=>'September','10'=>'Oktober','11'=>'Nopember','12'=>'Desember');
	$nama_hari  = array("Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu");
	
	$tanggal = "";
	if ($hari){
		$hari_num = date('N',strtotime($date));
		
		$tanggal .= $nama_hari[$hari_num-1].", ";
		
	}
	
	$split_tgl = explode("-",$date);
	$tanggal .= $split_tgl[2]." ".$bulan[$split_tgl[1]]." ".$split_tgl[0];
	return $tanggal;
}

function asset($asset)
{
	return base_url($asset);
}

function URL($url)
{
	return site_url($url);
}